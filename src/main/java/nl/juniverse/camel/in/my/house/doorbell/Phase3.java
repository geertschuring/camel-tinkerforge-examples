package nl.juniverse.camel.in.my.house.doorbell;

import org.apache.camel.CamelContext;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.impl.DefaultCamelContext;

public class Phase3 {
    public static void main(String[] args) throws Exception {

        CamelContext context = new DefaultCamelContext();
        context.addRoutes(new RouteBuilder() {
            public void configure() throws Exception {
                
                from("tinkerforge:/io16/io2?ioport=b&debounce=200", "tinkerforge:/io16/io9?ioport=a&debounce=200")
                    .log("unfiltered message")
                    .filter(simple("${date:now:HH} range '7..23'"))
                    .choice()
                        .when(body().isEqualTo(true))
                            .log("Reacting to button-down event")
                            .setHeader("duration", constant(50))
                            .to("tinkerforge:/solidstaterelay/ssr1")
                        .otherwise()
                            .log("Ignoring butten-up event");
                
            }
        });

        context.start();
        Thread.sleep(Long.MAX_VALUE);
    }
}


//.filter(body().isEqualTo(true))