package nl.juniverse.camel.in.my.house;

import org.apache.camel.CamelContext;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.impl.DefaultCamelContext;

public class LedstripBalance {
    public static void main(String[] args) throws Exception {

        CamelContext context = new DefaultCamelContext();

        context.addRoutes(new RouteBuilder() {
            @Override
            public void configure() throws Exception {

                int timer = 100;

                /*
                Time based messages load balanced over several LED lights
                 */
                from("timer://timer?period="+timer)
                    .setHeader("red",constant((short)0))
                    .setHeader("green",constant((short)0))
                    .setHeader("blue",constant((short)255))
                    .setHeader("duration",constant(timer))
                .loadBalance()
                    .roundRobin()
                        .to("direct:led0")
                        .to("direct:led1")
                        .to("direct:led2")
                        .to("direct:led3")
                        .to("direct:led4")
                        .to("direct:led5")
                        .to("direct:led6")
                        .to("direct:led7")
                        .to("direct:led8")
                        .to("direct:led9")
                        .to("direct:led10")
                        .to("direct:led11")
                        .to("direct:led12")
                        .to("direct:led13");

                from("direct:led")
                .to("tinkerforge:/ledstrip/Ls1?amountOfLeds=49&rgbPattern=bgr");

                /*
                LED routes
                 */
                from("direct:led0").setHeader("position",constant(0)).to("direct:led");
                from("direct:led1").setHeader("position",constant(1)).to("direct:led");
                from("direct:led2").setHeader("position",constant(2)).to("direct:led");
                from("direct:led3").setHeader("position",constant(3)).to("direct:led");
                from("direct:led4").setHeader("position",constant(4)).to("direct:led");
                from("direct:led5").setHeader("position",constant(5)).to("direct:led");
                from("direct:led6").setHeader("position",constant(6)).to("direct:led");
                from("direct:led7").setHeader("position",constant(7)).to("direct:led");
                from("direct:led8").setHeader("position",constant(8)).to("direct:led");
                from("direct:led9").setHeader("position",constant(9)).to("direct:led");
                from("direct:led10").setHeader("position",constant(10)).to("direct:led");
                from("direct:led11").setHeader("position",constant(11)).to("direct:led");
                from("direct:led12").setHeader("position",constant(12)).to("direct:led");
                from("direct:led13").setHeader("position",constant(13)).to("direct:led");

                
                
                
                
                
                
                
                
                
                
                
                
                

//                from("timer://timer?period="+timer)
//                    .setHeader("red",constant((short)255))
//                    .setHeader("green",constant((short)0))
//                    .setHeader("blue",constant((short)0))
//                    .setHeader("duration",constant(timer*3))
//                .loadBalance()
//                    .random()
//                        .to("direct:led14")
//                        .to("direct:led15")
//                        .to("direct:led16")
//                        .to("direct:led17")
//                        .to("direct:led18")
//                        .to("direct:led19")
//                        .to("direct:led20")
//                        .to("direct:led21")
//                        .to("direct:led22")
//                        .to("direct:led23")
//                        .to("direct:led24")
//                        .to("direct:led25")
//                        .to("direct:led26")
//                        .to("direct:led27");
                
                
                from("direct:led14").setHeader("position",constant(14)).to("direct:led");
                from("direct:led15").setHeader("position",constant(15)).to("direct:led");
                from("direct:led16").setHeader("position",constant(16)).to("direct:led");
                from("direct:led17").setHeader("position",constant(17)).to("direct:led");
                from("direct:led18").setHeader("position",constant(18)).to("direct:led");
                from("direct:led19").setHeader("position",constant(19)).to("direct:led");
                from("direct:led20").setHeader("position",constant(20)).to("direct:led");
                from("direct:led21").setHeader("position",constant(21)).to("direct:led");
                from("direct:led22").setHeader("position",constant(22)).to("direct:led");
                from("direct:led23").setHeader("position",constant(23)).to("direct:led");
                from("direct:led24").setHeader("position",constant(24)).to("direct:led");
                from("direct:led25").setHeader("position",constant(25)).to("direct:led");
                from("direct:led26").setHeader("position",constant(26)).to("direct:led");
                from("direct:led27").setHeader("position",constant(27)).to("direct:led");
            }
        });

        context.start();

        Thread.sleep(Long.MAX_VALUE);
    }
}