package nl.juniverse.camel.in.my.house;

import org.apache.camel.CamelContext;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.impl.DefaultCamelContext;

public class TemperaturesOnLCD {
    public static void main(String[] args) throws Exception {

        CamelContext context = new DefaultCamelContext();

        context.addRoutes(new RouteBuilder() {
            public void configure() throws Exception {

                from("tinkerforge:/temperature/T9")
                    .setHeader("line", constant("0"))
                    .setBody(simple("Temperature: ${body}"))
                    .to("direct:lcd");

                from("tinkerforge:/humidity/H9")
                    .setHeader("line", constant("1"))
                    .setBody(simple("Humidity: ${body}"))
                    .to("direct:lcd");

                from("direct:lcd")
                    .to("tinkerforge:/lcd20x4/LCD9");

            }
        });

        context.start();
        Thread.sleep(Long.MAX_VALUE);
    }
}
