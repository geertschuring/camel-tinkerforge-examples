package nl.juniverse.camel.in.my.house.doorbell;

import org.apache.camel.CamelContext;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.impl.DefaultCamelContext;

public class Phase1 {
    public static void main(String[] args) throws Exception {

        CamelContext context = new DefaultCamelContext();
        context.addRoutes(new RouteBuilder() {
            public void configure() throws Exception {
                
                from("tinkerforge:/io16/io2?ioport=b")
                .to("log:default")
                
            ;}
        });

        context.start();
        Thread.sleep(Long.MAX_VALUE);
    }
}


// &debounce=200
// .to("tinkerforge:/solidstaterelay/ssr1") // bell
// .to("tinkerforge:/solidstaterelay/ssr2") // light
// .from("tinkerforge:/io16/io9?ioport=a")  // ikea box
