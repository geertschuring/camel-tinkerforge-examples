package nl.juniverse.camel.in.my.house.ledmatrix;

import org.apache.camel.CamelContext;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.impl.DefaultCamelContext;

public class LedMatrixDemo {
    public static void main(String[] args) throws Exception {

        CamelContext context = new DefaultCamelContext();
        context.addRoutes(new RouteBuilder() {
            public void configure() throws Exception {
                
                from("timer:default?period=1000")
                .setHeader("green", constant("255"))
                .setBody().simple("${date:now:ss}")
                .to("log:default")
                .to("tinkerforge:/ledstrip/Ls1?amountOfLeds=49&rgbPattern=bgr&modus=charactermatrix&layout=7x7");
                
                
                
            }
        });
        context.start();
        Thread.sleep(Long.MAX_VALUE);
    }
}






//from("tinkerforge:/voltagecurrent/VC1?interval=100")
//.filter(header("com.tinkerforge.bricklet.voltagecurrent.type").isEqualTo("power"))
//.to("log:default");



//String host = "localhost";

//from("tinkerforge://"+host+"/temperature/T9")
//.setHeader("blue", constant("100"))
//.to("log:default")
//.to("tinkerforge://"+host+"/ledstrip/LS1?chipType=2812&amountOfLeds=150&rgbPattern=brg&modus=charactermatrix&layout=30x5");
//