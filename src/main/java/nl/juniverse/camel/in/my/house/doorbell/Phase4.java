package nl.juniverse.camel.in.my.house.doorbell;

import org.apache.camel.CamelContext;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.impl.DefaultCamelContext;

public class Phase4 {
    public static void main(String[] args) throws Exception {

        CamelContext context = new DefaultCamelContext();
        context.addRoutes(new RouteBuilder() {
            public void configure() throws Exception {

                from("tinkerforge:/io16/io2?ioport=b&debounce=200", "tinkerforge:/io16/io9?ioport=a&debounce=200")
                .to("activemq:queue:doorbell");
                
                from("activemq:queue:doorbell")
                .filter(body().isEqualTo(true))
                .filter(simple("${date:now:HH} range '7..23'"))
                .throttle(1).timePeriodMillis(1000)
                .setHeader("duration", constant(20))
                .to("tinkerforge:/solidstaterelay/ssr1")
            ;}
        });

        context.start();
        Thread.sleep(Long.MAX_VALUE);
    }
}








//from("tinkerforge:/io16/io2?ioport=b&debounce=200", "tinkerforge:/io16/io9?ioport=a&debounce=100")
//.inOnly("activemq:queue:doorbell?timeToLive=100");
//
//

