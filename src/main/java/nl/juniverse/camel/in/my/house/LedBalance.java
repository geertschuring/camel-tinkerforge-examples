package nl.juniverse.camel.in.my.house;

import org.apache.camel.CamelContext;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.impl.DefaultCamelContext;

public class LedBalance {
    public static void main(String[] args) throws Exception {

        CamelContext context = new DefaultCamelContext();

        context.addRoutes(new RouteBuilder() {
            @Override
            public void configure() throws Exception {

                /*
                Time based messages load balanced over several LED lights
                 */
                from("timer://timer?period=200")
                    .setBody(constant("on"))
                    .setHeader("duration",constant("200"))
                    .loadBalance()
                        .roundRobin()
                            .to("direct:led0")
                            .to("direct:led1")
                            .to("direct:led2")
                            .to("direct:led3")
                            .to("direct:led4")
                            .to("direct:led5")
                            .to("direct:led6")
                            .to("direct:led7")
                ;

                /*
                LED routes
                 */
                from("direct:led").to("tinkerforge:/io16/io9?ioport=b");
                from("direct:led0").setHeader("iopin",constant(0)).to("direct:led");
                from("direct:led1").setHeader("iopin",constant(1)).to("direct:led");
                from("direct:led2").setHeader("iopin",constant(2)).to("direct:led");
                from("direct:led3").setHeader("iopin",constant(3)).to("direct:led");
                from("direct:led4").setHeader("iopin",constant(4)).to("direct:led");
                from("direct:led5").setHeader("iopin",constant(5)).to("direct:led");
                from("direct:led6").setHeader("iopin",constant(6)).to("direct:led");
                from("direct:led7").setHeader("iopin",constant(7)).to("direct:led");
            }
        });

        context.start();

        Thread.sleep(Long.MAX_VALUE);
    }
}