package nl.juniverse.camel.in.my.house;

import org.apache.camel.CamelContext;
import org.apache.camel.Exchange;
import org.apache.camel.Message;
import org.apache.camel.Processor;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.impl.DefaultCamelContext;

/**
 * Created with IntelliJ IDEA.
 * User: Geert Schuring
 * Date: 09/11/14
 */
public class PotentioLedDemo {
    public static void main(String[] args) throws Exception {

        final Processor ledInputProcessor = new Processor() {
            final float factor = 255f/100;
            short red, green, blue;
            @Override
            public void process(Exchange exchange) throws Exception {
                short value = (short) (exchange.getIn().getBody(Integer.class) * factor);
                String color = exchange.getIn().getHeader("color", String.class);
                switch(color) {
                    case "red"      : red = value;       break;
                    case "green"    : green = value;     break;
                    case "blue"     : blue = value;      break;
                    default         : return;
                }
                Message message = exchange.getOut();
                message.setHeader("red", red);
                message.setHeader("green", green);
                message.setHeader("blue", blue);
            }
        };

        CamelContext context = new DefaultCamelContext();
        context.addRoutes(new RouteBuilder() {
            public void configure() throws Exception {

                int interval = 50;

                from("tinkerforge:/linearpoti/Lp1?interval="+interval)
                    .setHeader("color", constant("red"))
                    .to("direct:ledstrip");

                from("tinkerforge:/linearpoti/Lp2?interval="+interval)
                    .setHeader("color", constant("green"))
                    .to("direct:ledstrip");

                from("tinkerforge:/linearpoti/Lp3?interval="+interval)
                    .setHeader("color", constant("blue"))
                    .to("direct:ledstrip");

                from("direct:ledstrip")
                    .process(ledInputProcessor)
                    .to("log:default?showHeaders=true")
                    .to("tinkerforge:/ledstrip/Ls1?amountOfLeds=49&rgbPattern=bgr");
                ;
            }
        });
        context.start();
        Thread.sleep(Long.MAX_VALUE);
    }
}
