package nl.juniverse.camel.tinkerforge.demo;

import org.apache.camel.CamelContext;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.impl.DefaultCamelContext;

public class PiezoSpeakerDemo {
    public static void main(String[] args) throws Exception {

        CamelContext context = new DefaultCamelContext();

        context.addRoutes(new RouteBuilder() {
            public void configure() throws Exception {

            from("timer:default?period=500")
            .to("tinkerforge:/piezospeaker/ps1?frequency=1000&duration=100");
            }
        });

        context.start();
        Thread.sleep(Long.MAX_VALUE);
    }
}