package nl.juniverse.camel.tinkerforge.demo;

import com.tinkerforge.*;

import java.io.IOException;

class FoobarListener implements IPConnection.EnumerateListener, BrickletRotaryPoti.PositionListener {
	private IPConnection ipcon;
	private String expectedUid;
	private BrickletRotaryPoti bricklet;

	public FoobarListener(IPConnection ipcon, String expectedUid) {
		this.ipcon = ipcon;
		this.expectedUid = expectedUid;
	}

	public void enumerate(String uid, String connectedUid, char position,
	                      short[] hardwareVersion, short[] firmwareVersion,
	                      int deviceIdentifier, short enumerationType) {
		if (enumerationType != IPConnection.ENUMERATION_TYPE_AVAILABLE) return;
		if (deviceIdentifier != BrickletRotaryPoti.DEVICE_IDENTIFIER) return;
		if (!uid.equals(expectedUid)) return;

		bricklet = new BrickletRotaryPoti(uid, ipcon); // Create device object

		try {
			bricklet.setPositionCallbackPeriod(100);
		} catch (TinkerforgeException e) {
			e.printStackTrace();
		}

		bricklet.addPositionListener(this);
	}

	public void position(short position) {
		System.out.println(expectedUid + ": " + position);
	}
}

public class RotaryPotiTest {
	private static final String HOST = "localhost";
	private static final int PORT = 4223;
	private static final String UID1 = "rp1";
	private static final String UID2 = "rp2";
	private static final String UID3 = "rp3";

	public static void main(String args[]) throws Exception {

        Runnable runnable1 = new Runnable() {
            @Override
            public void run() {
                try {
                    IPConnection ipcon1 = new IPConnection();
                    ipcon1.connect(HOST, PORT);
                    ipcon1.addEnumerateListener(new FoobarListener(ipcon1, UID1));
                    ipcon1.enumerate();
                } catch (IOException | AlreadyConnectedException | NotConnectedException e) {
                    e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                }
            }
        };

        Runnable runnable2 = new Runnable() {
            @Override
            public void run() {
                try {
                    IPConnection ipcon2 = new IPConnection();
                    ipcon2.connect(HOST, PORT);
                    ipcon2.addEnumerateListener(new FoobarListener(ipcon2, UID2));
                    ipcon2.enumerate();
                } catch (IOException | AlreadyConnectedException | NotConnectedException e) {
                    e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                }
            }
        };

        Runnable runnable3 = new Runnable() {
            @Override
            public void run() {
                try {
                    IPConnection ipcon3 = new IPConnection();
                    ipcon3.connect(HOST, PORT);
                    ipcon3.addEnumerateListener(new FoobarListener(ipcon3, UID3));
                    ipcon3.enumerate();
                } catch (IOException | AlreadyConnectedException | NotConnectedException e) {
                    e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                }
            }
        };


        Thread thread1 = new Thread(runnable1);
        Thread thread2 = new Thread(runnable2);
        Thread thread3 = new Thread(runnable3);

        thread1.start();
        thread2.start();
        thread3.start();

		System.out.println("Press key to exit"); System.in.read();

	}
}
