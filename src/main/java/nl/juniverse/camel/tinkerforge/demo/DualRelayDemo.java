package nl.juniverse.camel.tinkerforge.demo;

import org.apache.camel.CamelContext;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.impl.DefaultCamelContext;

public class DualRelayDemo {
    public static void main(String[] args) throws Exception {

        CamelContext context = new DefaultCamelContext();

        context.addRoutes(new RouteBuilder() {
            public void configure() throws Exception {

            from("timer:default?period=2000")
            .setHeader("duration", constant(1000))
            .setBody(constant("on"))
            .to("tinkerforge:/dualrelay/dr1");
            }
        });

        context.start();
        Thread.sleep(Long.MAX_VALUE);
    }
}