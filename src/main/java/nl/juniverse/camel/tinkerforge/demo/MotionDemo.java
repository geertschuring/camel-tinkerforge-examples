package nl.juniverse.camel.tinkerforge.demo;

import org.apache.camel.CamelContext;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.impl.DefaultCamelContext;

public class MotionDemo {
    public static void main(String[] args) throws Exception {

        CamelContext context = new DefaultCamelContext();

        context.addRoutes(new RouteBuilder() {
            public void configure() throws Exception {

                from("tinkerforge:/motion/M2")
                .to("log:output?showHeaders=true");
            }
        });

        context.start();
        Thread.sleep(Long.MAX_VALUE);
    }
}

