package nl.juniverse.camel.tinkerforge.demo;

import org.apache.camel.CamelContext;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.impl.DefaultCamelContext;

public class Buttons {
    public static void main(String[] args) throws Exception {

        CamelContext context = new DefaultCamelContext();

        context.addRoutes(new RouteBuilder() {
            @Override
            public void configure() throws Exception {

                from("tinkerforge:/io16/io9&ioport=a")
                .setHeader("iopin").simple("${header[com.tinkerforge.bricklet.io16.pin]}")
                .to("log:output?showHeaders=true")
                .to("tinkerforge:io16?uid=io9&ioport=b");
            }
        });

        context.start();

        Thread.sleep(Long.MAX_VALUE);
    }
}

