package nl.juniverse.camel.tinkerforge.demo;

import org.apache.camel.CamelContext;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.impl.DefaultCamelContext;

public class PotentiometersDemo {
    public static void main(String[] args) throws Exception {

        CamelContext context = new DefaultCamelContext();

        context.addRoutes(new RouteBuilder() {
            public void configure() throws Exception {

                int interval = 100;

                from("tinkerforge:rotarypoti?uid=rp1&interval="+interval)
                .to("log:output?showHeaders=true");

                from("tinkerforge:rotarypoti?uid=rp2&interval="+interval)
                .to("log:output?showHeaders=true");

                from("tinkerforge:rotarypoti?uid=rp3&interval="+interval)
                .to("log:output?showHeaders=true");


                from("tinkerforge:linearpoti?uid=Lp1&interval="+interval)
                .to("log:output?showHeaders=true");

                from("tinkerforge:linearpoti?uid=Lp2&interval="+interval)
                .to("log:output?showHeaders=true");

                from("tinkerforge:linearpoti?uid=Lp3&interval=" + interval)
                .to("log:output?showHeaders=true");
            }
        });

        context.start();
        Thread.sleep(Long.MAX_VALUE);
    }
}